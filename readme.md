This script read the code and try to evaluate the solution.   
It i built using python ply and lex.  
Input file must be formated.  

Statement Supported:    
- Block statement  
- condition statement  
- ptint statement  
- Assignment statement  


An example input script might look like this: 
{ 
number = 33;  
isPrime = 1;  
i = 2;  
while(isPrime==1 and i  
if (number%i==0) {  
isPrime = 0;  
}  
i = i + 1;  
}  
if(isPrime==1){  
print("isPrime is true");  
} else {  
print("isPrime is false");  
}  
}  
The output from this file should be only:  
isPrime is false  

Examle 2 Input  
{print("a");}  
the output should be:  
a  

Example 3 Input  
{print([1, "a", 2]);}  
the output should be:  
[1, 'a', 2]  

Run using  
python3 main.py inputProgram.txt  